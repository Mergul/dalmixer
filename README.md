DALmixer
==========

A dynamic binding to ALmixer for the D Programming Language.

Library site: "http://playcontrol.net/opensource/ALmixer/"

Sample code:

```D
import dalmixer;

void main() {
    // Load the ALmixer library.
    DALmixer.load();

    // Now ALmixer functions can be called.
    ...
}
```

Tested on windows and linux.

Prebuild binary files "libALmixer.dll" "libALmixer.so" are included to bin folder.