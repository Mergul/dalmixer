/*
    ALmixer:  A library to make playing pre-loaded sounds and streams easier
	with high performance and potential access to OpenAL effects.
    Copyright 2002, 2010 Eric Wing <ewing . public @ playcontrol.net>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public
    License along with this library; if not, write to the Free
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

module dalmixer;

import derelict.openal.al;
import derelict.util.loader;
import derelict.util.system;

private {
	static if( Derelict_OS_Windows )
		enum libNames = "ALmixer.dll, libALmixer";
	/*else static if( Derelict_OS_Mac )
		enum libNames = "../Frameworks/ALmixer.framework/ALmixer, /Library/Frameworks/ALmixer.framework/ALmixer, /System/Library/Frameworks/ALmixer.framework/ALmixer";*/
	else static if( Derelict_OS_Posix )
		enum libNames = "ALmixer.so, libALmixer.so, libALmixer.so.0.4.0";
	else
		static assert( 0, "Need to implement ALmixer libNames for this operating system." );
}

struct ALmixer_Data;
struct ALmixer_RWops;

struct ALmixer_AudioInfo
{
	ALushort format;
	ALubyte channels;
	ALuint rate;
};

struct ALmixer_version
{
	ALubyte major;
	ALubyte minor;
	ALubyte patch;
};

enum
{
	int ALMIXER_MAJOR_VERSION = 0,
	int ALMIXER_MINOR_VERSION = 4,
	int ALMIXER_PATCHLEVEL	= 0,
	
	int ALMIXER_DEFAULT_FREQUENCY = 0,
	int ALMIXER_DEFAULT_REFRESH = 0,
	int ALMIXER_DEFAULT_NUM_CHANNELS	= 32,
	int ALMIXER_DEFAULT_NUM_SOURCES = ALMIXER_DEFAULT_NUM_CHANNELS,
	
	int ALMIXER_DEFAULT_BUFFERSIZE = 8192,
	int ALMIXER_DEFAULT_PREDECODED_BUFFERSIZE = 4096,
	int ALMIXER_DEFAULT_QUEUE_BUFFERS = 12,
	int ALMIXER_DEFAULT_STARTUP_BUFFERS = 4,
	int ALMIXER_DEFAULT_BUFFERS_TO_QUEUE_PER_UPDATE_PASS = 2
}

//alias dal_ALmixer_Init = function();

extern( C ) @nogc nothrow {

	///CoreOperation Initialization, Tear-down, and Core Operational Commands

	alias dal_ALmixer_GetLinkedVersion = const ALmixer_version* function();
	alias dal_ALmixer_GetError = const char* function();
	alias dal_ALmixer_SetError = void function(const char *fmt, ...);
	alias dal_ALmixer_GetTicks = ALuint function();
	alias dal_ALmixer_Delay = void function(ALuint milliseconds_delay);
	alias dal_ALmixer_Init = ALboolean function( ALuint playback_frequency, ALuint num_sources, ALuint refresh_rate );
	alias dal_ALmixer_InitContext = ALboolean function(ALuint playback_frequency, ALuint refresh_rate);
	alias dal_ALmixer_InitMixer = ALboolean function(ALuint num_sources);
	alias dal_ALmixer_BeginInterruption = void function();
	alias dal_ALmixer_EndInterruption = void function();
	alias dal_ALmixer_IsInInterruption = ALboolean function();
	alias dal_ALmixer_SuspendUpdates = void function();
	alias dal_ALmixer_ResumeUpdates = void function();
	alias dal_ALmixer_AreUpdatesSuspended = ALboolean function();
	alias dal_ALmixer_SuspendPlayingState = void function();
	alias dal_ALmixer_ResumePlayingState = void function();
	alias dal_ALmixer_IsPlayingStateSuspended = ALboolean function();
	alias dal_ALmixer_Quit = void function();
	alias dal_ALmixer_QuitWithoutFreeData = void function();
	alias dal_ALmixer_IsInitialized = ALboolean function();
	alias dal_ALmixer_GetFrequency = ALuint function();
	alias dal_ALmixer_AllocateChannels = ALint function(ALint num_chans);
	alias dal_ALmixer_ReserveChannels = ALint function(ALint number_of_reserve_channels);
	alias dal_ALmixer_Update = ALint function();

	///LoadAPI Load Audio Functions

	alias dal_ALmixer_LoadSample_RW = ALmixer_Data* function(ALmixer_RWops* rw_ops, const char* file_ext, ALuint buffer_size, ALboolean decode_mode_is_predecoded, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadStream_RW = ALmixer_Data* function(ALmixer_RWops* rw_ops, const char* file_ext, ALuint buffer_size, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadAll_RW = ALmixer_Data* function(ALmixer_RWops* rw_ops, const char* file_ext, ALuint access_data);
	alias dal_ALmixer_LoadSample = ALmixer_Data * function(const char* file_name, ALuint buffer_size, ALboolean decode_mode_is_predecoded, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadStream = ALmixer_Data* function(const char* file_name, ALuint buffer_size, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadAll = ALmixer_Data* function(const char* file_name, ALuint access_data);
	alias dal_ALmixer_LoadSample_RAW_RW = ALmixer_Data* function(ALmixer_RWops* rw_ops, const char* file_ext, ALmixer_AudioInfo* desired_format, ALuint buffer_size, ALboolean decode_mode_is_predecoded, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadStream_RAW_RW = ALmixer_Data* function(ALmixer_RWops* rw_ops, const char* file_ext, ALmixer_AudioInfo* desired_format, ALuint buffer_size, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadAll_RAW_RW = ALmixer_Data* function(ALmixer_RWops* rw_ops, const char* file_ext, ALmixer_AudioInfo* desired_format, ALuint access_data);
	alias dal_ALmixer_LoadSample_RAW = ALmixer_Data* function(const char* file_name, ALmixer_AudioInfo* desired_format, ALuint buffer_size, ALboolean decode_mode_is_predecoded, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadStream_RAW = ALmixer_Data* function(const char* file_name, ALmixer_AudioInfo* desired_format, ALuint buffer_size, ALuint max_queue_buffers, ALuint num_startup_buffers, ALuint suggested_number_of_buffers_to_queue_per_update_pass, ALuint access_data);
	alias dal_ALmixer_LoadAll_RAW = ALmixer_Data* function(const char* file_name, ALmixer_AudioInfo* desired_format, ALuint access_data);
	alias dal_ALmixer_FreeData = void function(ALmixer_Data* functionalmixer_data);
	alias dal_ALmixer_IsPredecoded = ALboolean function(ALmixer_Data* functionalmixer_data);

	///CallbackAPI Callbacks

	alias void function(ALint which_channel, ALuint al_source, ALmixer_Data* almixer_data, ALboolean finished_naturally, void* ALmixer_SetPlaybackFinishedCallbackContainer)playback_finished_callback;
	alias dal_ALmixer_SetPlaybackFinishedCallback = void function(void function(ALint which_channel, ALuint al_source, ALmixer_Data* almixer_data, ALboolean finished_naturally, void* user_data)playback_finished_callback, void* user_data);
	alias dal_ALmixer_SetPlaybackDataCallback = void function(void function(ALint which_channel, ALuint al_source, ALbyte* pcm_data, ALuint num_bytes, ALuint frequency, ALubyte num_channels_in_sample, ALubyte bit_depth, ALboolean is_unsigned, ALboolean decode_mode_is_predecoded, ALuint length_in_msec, void* user_data), void* user_data);

	///PlayAPI Functions useful for playback.

	alias dal_ALmixer_GetTotalTime = ALint function(ALmixer_Data* almixer_data);
	alias dal_ALmixer_GetSource = ALuint function(ALint which_channel);
	alias dal_ALmixer_GetChannel = ALint function(ALuint al_source);
	alias dal_ALmixer_FindFreeChannel = ALint function(ALint start_channel);
	alias dal_ALmixer_PlayChannelTimed = ALint function(ALint which_channel, ALmixer_Data* almixer_data, ALint number_of_loops, ALint number_of_milliseconds);
	alias dal_ALmixer_PlayChannel = ALint function(ALint which_channel, ALmixer_Data* almixer_data, ALint number_of_loops);
	alias dal_ALmixer_PlaySourceTimed = ALuint function(ALuint al_source, ALmixer_Data* almixer_data, ALint number_of_loops, ALint number_of_milliseconds);
	alias dal_ALmixer_PlaySource = ALuint function(ALuint al_source, ALmixer_Data* almixer_data, ALint number_of_loops);
	alias dal_ALmixer_HaltChannel = ALint function(ALint which_channel);
	alias dal_ALmixer_HaltSource = ALint function(ALuint al_source);
	alias dal_ALmixer_RewindData = ALboolean function(ALmixer_Data* almixer_data);
	alias dal_ALmixer_RewindChannel = ALint function(ALint which_channel);
	alias dal_ALmixer_RewindSource = ALint function(ALuint al_source);
	alias dal_ALmixer_SeekData = ALboolean function(ALmixer_Data* almixer_data, ALuint msec_pos);
	alias dal_ALmixer_SeekChannel = ALint function(ALint which_channel, ALuint msec_pos);
	alias dal_ALmixer_SeekSource = ALint function(ALuint al_source, ALuint msec_pos);
	alias dal_ALmixer_PauseChannel = ALint function(ALint which_channel);
	alias dal_ALmixer_PauseSource = ALint function(ALuint al_source);
	alias dal_ALmixer_ResumeChannel = ALint function(ALint which_channel);
	alias dal_ALmixer_ResumeSource = ALint function(ALuint al_source);
	alias dal_ALmixer_ExpireChannel = ALint function(ALint which_channel, ALint number_of_milliseconds);
	alias dal_ALmixer_ExpireSource = ALint function(ALuint al_source, ALint number_of_milliseconds);

	///VolumeAPI Volume and Fading

	alias dal_ALmixer_FadeInChannelTimed = ALint function(ALint which_channel, ALmixer_Data* almixer_data, ALint number_of_loops, ALuint fade_ticks, ALint expire_ticks);
	alias dal_ALmixer_FadeInChannel = ALint function(ALint which_channel, ALmixer_Data* almixer_data, ALint number_of_loops, ALuint fade_ticks);
	alias dal_ALmixer_FadeInSourceTimed = ALuint function(ALuint al_source, ALmixer_Data* almixer_data, ALint number_of_loops, ALuint fade_ticks, ALint expire_ticks);
	alias dal_ALmixer_FadeInSource = ALuint function(ALuint al_source, ALmixer_Data* almixer_data, ALint number_of_loops, ALuint fade_ticks);
	alias dal_ALmixer_FadeOutChannel = ALint function(ALint which_channel, ALuint fade_ticks);
	alias dal_ALmixer_FadeOutSource = ALint function(ALuint al_source, ALuint fade_ticks);
	alias dal_ALmixer_FadeChannel = ALint function(ALint which_channel, ALuint fade_ticks, ALfloat volume);
	alias dal_ALmixer_FadeSource = ALint function(ALuint al_source, ALuint fade_ticks, ALfloat volume);
	alias dal_ALmixer_SetVolumeChannel = ALboolean function(ALint which_channel, ALfloat volume);
	alias dal_ALmixer_SetVolumeSource = ALboolean function(ALuint al_source, ALfloat volume);
	alias dal_ALmixer_GetVolumeChannel = ALfloat function(ALint which_channel);
	alias dal_ALmixer_GetVolumeSource = ALfloat function(ALuint al_source);
	alias dal_ALmixer_SetMaxVolumeChannel = ALboolean function(ALint which_channel, ALfloat volume);
	alias dal_ALmixer_SetMaxVolumeSource = ALboolean function(ALuint al_source, ALfloat volume);
	alias dal_ALmixer_GetMaxVolumeChannel = ALfloat function(ALint which_channel);
	alias dal_ALmixer_GetMaxVolumeSource = ALfloat function(ALuint al_source);
	alias dal_ALmixer_SetMinVolumeChannel = ALboolean function(ALint which_channel, ALfloat volume);
	alias dal_ALmixer_SetMinVolumeSource = ALboolean function(ALuint al_source, ALfloat volume);
	alias dal_ALmixer_GetMinVolumeChannel = ALfloat function(ALint which_channel);
	alias dal_ALmixer_GetMinVolumeSource = ALfloat function(ALuint al_source);
	alias dal_ALmixer_SetMasterVolume = ALboolean function(ALfloat new_volume);
	alias dal_ALmixer_GetMasterVolume = ALfloat function();
	
	///QueryAPI Query APIs

	alias dal_ALmixer_IsActiveSource = ALint function(ALuint al_source);
	alias dal_ALmixer_IsPlayingChannel = ALint function(ALint which_channel);
	alias dal_ALmixer_IsPlayingSource = ALint function(ALuint al_source);
	alias dal_ALmixer_IsPausedChannel = ALint function(ALint which_channel);
	alias dal_ALmixer_IsPausedSource = ALint function(ALuint al_source);
	alias dal_ALmixer_CountAllFreeChannels = ALuint function();
	alias dal_ALmixer_CountUnreservedFreeChannels = ALuint function();
	alias dal_ALmixer_CountAllUsedChannels = ALuint function();
	alias dal_ALmixer_CountUnreservedUsedChannels = ALuint function();
	alias dal_ALmixer_CountTotalChannels = ALuint function();
	alias dal_ALmixer_CountReservedChannels = ALuint function();
	
	///DebugAPI Debug APIs

	alias dal_ALmixer_OutputDecoders = void function();
	alias dal_ALmixer_OutputOpenALInfo = void function();
	alias dal_ALmixer_CompiledWithThreadBackend = ALboolean function();
}

__gshared {

	///CoreOperation Initialization, Tear-down, and Core Operational Commands

	dal_ALmixer_GetLinkedVersion ALmixer_GetLinkedVersion;
	dal_ALmixer_GetError ALmixer_GetError;
	dal_ALmixer_SetError ALmixer_SetError;
	dal_ALmixer_GetTicks ALmixer_GetTicks;
	dal_ALmixer_Delay ALmixer_Delay;
	dal_ALmixer_Init ALmixer_Init;
	dal_ALmixer_InitContext ALmixer_InitContext;
	dal_ALmixer_InitMixer ALmixer_InitMixer;
	dal_ALmixer_BeginInterruption ALmixer_BeginInterruption;
	dal_ALmixer_EndInterruption ALmixer_EndInterruption;
	dal_ALmixer_IsInInterruption ALmixer_IsInInterruption;
	dal_ALmixer_SuspendUpdates ALmixer_SuspendUpdates;
	dal_ALmixer_ResumeUpdates ALmixer_ResumeUpdates;
	dal_ALmixer_AreUpdatesSuspended ALmixer_AreUpdatesSuspended;
	dal_ALmixer_SuspendPlayingState ALmixer_SuspendPlayingState;
	dal_ALmixer_ResumePlayingState ALmixer_ResumePlayingState;
	dal_ALmixer_IsPlayingStateSuspended ALmixer_IsPlayingStateSuspended;
	dal_ALmixer_Quit ALmixer_Quit;
	dal_ALmixer_QuitWithoutFreeData ALmixer_QuitWithoutFreeData;
	dal_ALmixer_IsInitialized ALmixer_IsInitialized;
	dal_ALmixer_GetFrequency ALmixer_GetFrequency;
	dal_ALmixer_AllocateChannels ALmixer_AllocateChannels;
	dal_ALmixer_ReserveChannels ALmixer_ReserveChannels;
	dal_ALmixer_Update ALmixer_Update;

	///LoadAPI Load Audio Functions

	dal_ALmixer_LoadSample_RW ALmixer_LoadSample_RW;
	dal_ALmixer_LoadStream_RW ALmixer_LoadStream_RW;
	dal_ALmixer_LoadAll_RW ALmixer_LoadAll_RW;
	dal_ALmixer_LoadSample ALmixer_LoadSample;
	dal_ALmixer_LoadStream ALmixer_LoadStream;
	dal_ALmixer_LoadAll ALmixer_LoadAll;
	dal_ALmixer_LoadSample_RAW_RW ALmixer_LoadSample_RAW_RW; 
	dal_ALmixer_LoadStream_RAW_RW ALmixer_LoadStream_RAW_RW;
	dal_ALmixer_LoadAll_RAW_RW ALmixer_LoadAll_RAW_RW;
	dal_ALmixer_LoadSample_RAW ALmixer_LoadSample_RAW;
	dal_ALmixer_LoadStream_RAW ALmixer_LoadStream_RAW;
	dal_ALmixer_LoadAll_RAW ALmixer_LoadAll_RAW;
	dal_ALmixer_FreeData ALmixer_FreeData;
	dal_ALmixer_IsPredecoded ALmixer_IsPredecoded;

	///CallbackAPI Callbacks

	dal_ALmixer_SetPlaybackFinishedCallback ALmixer_SetPlaybackFinishedCallback;
	dal_ALmixer_SetPlaybackDataCallback ALmixer_SetPlaybackDataCallback;

	///PlayAPI Functions useful for playback.

	dal_ALmixer_GetTotalTime ALmixer_GetTotalTime;
	dal_ALmixer_GetSource ALmixer_GetSource;
	dal_ALmixer_GetChannel ALmixer_GetChannel;
	dal_ALmixer_FindFreeChannel ALmixer_FindFreeChannel;
	dal_ALmixer_PlayChannelTimed ALmixer_PlayChannelTimed;
	dal_ALmixer_PlayChannel ALmixer_PlayChannel;
	dal_ALmixer_PlaySourceTimed ALmixer_PlaySourceTimed;
	dal_ALmixer_PlaySource ALmixer_PlaySource;
	dal_ALmixer_HaltChannel ALmixer_HaltChannel;
	dal_ALmixer_HaltSource ALmixer_HaltSource;
	dal_ALmixer_RewindData ALmixer_RewindData;
	dal_ALmixer_RewindChannel ALmixer_RewindChannel;
	dal_ALmixer_RewindSource ALmixer_RewindSource;
	dal_ALmixer_SeekData ALmixer_SeekData;
	dal_ALmixer_SeekChannel ALmixer_SeekChannel;
	dal_ALmixer_SeekSource ALmixer_SeekSource;
	dal_ALmixer_PauseChannel ALmixer_PauseChannel;
	dal_ALmixer_PauseSource ALmixer_PauseSource;
	dal_ALmixer_ResumeChannel ALmixer_ResumeChannel;
	dal_ALmixer_ResumeSource ALmixer_ResumeSource;
	dal_ALmixer_ExpireChannel ALmixer_ExpireChannel;
	dal_ALmixer_ExpireSource ALmixer_ExpireSource;

	///VolumeAPI Volume and Fading

	dal_ALmixer_FadeInChannelTimed ALmixer_FadeInChannelTimed;
	dal_ALmixer_FadeInChannel ALmixer_FadeInChannel;
	dal_ALmixer_FadeInSourceTimed ALmixer_FadeInSourceTimed;
	dal_ALmixer_FadeInSource ALmixer_FadeInSource;
	dal_ALmixer_FadeOutChannel ALmixer_FadeOutChannel;
	dal_ALmixer_FadeOutSource ALmixer_FadeOutSource;
	dal_ALmixer_FadeChannel ALmixer_FadeChannel;
	dal_ALmixer_FadeSource ALmixer_FadeSource;
	dal_ALmixer_SetVolumeChannel ALmixer_SetVolumeChannel;
	dal_ALmixer_SetVolumeSource ALmixer_SetVolumeSource;
	dal_ALmixer_GetVolumeChannel ALmixer_GetVolumeChannel;
	dal_ALmixer_GetVolumeSource ALmixer_GetVolumeSource;
	dal_ALmixer_SetMaxVolumeChannel ALmixer_SetMaxVolumeChannel;
	dal_ALmixer_SetMaxVolumeSource ALmixer_SetMaxVolumeSource;
	dal_ALmixer_GetMaxVolumeChannel ALmixer_GetMaxVolumeChannel;
	dal_ALmixer_GetMaxVolumeSource ALmixer_GetMaxVolumeSource;
	dal_ALmixer_SetMinVolumeChannel ALmixer_SetMinVolumeChannel;
	dal_ALmixer_SetMinVolumeSource ALmixer_SetMinVolumeSource;
	dal_ALmixer_GetMinVolumeChannel ALmixer_GetMinVolumeChannel;
	dal_ALmixer_GetMinVolumeSource ALmixer_GetMinVolumeSource;
	dal_ALmixer_SetMasterVolume ALmixer_SetMasterVolume;
	dal_ALmixer_GetMasterVolume ALmixer_GetMasterVolume;

	///QueryAPI Query APIs

	dal_ALmixer_IsActiveSource ALmixer_IsActiveSource;
	dal_ALmixer_IsPlayingChannel ALmixer_IsPlayingChannel;
	dal_ALmixer_IsPlayingSource ALmixer_IsPlayingSource;
	dal_ALmixer_IsPausedChannel ALmixer_IsPausedChannel;
	dal_ALmixer_IsPausedSource ALmixer_IsPausedSource;
	dal_ALmixer_CountAllFreeChannels ALmixer_CountAllFreeChannels;
	dal_ALmixer_CountUnreservedFreeChannels ALmixer_CountUnreservedFreeChannels;
	dal_ALmixer_CountAllUsedChannels ALmixer_CountAllUsedChannels;
	dal_ALmixer_CountUnreservedUsedChannels ALmixer_CountUnreservedUsedChannels;
	dal_ALmixer_CountTotalChannels ALmixer_CountTotalChannels;
	dal_ALmixer_CountReservedChannels ALmixer_CountReservedChannels;

	///DebugAPI Debug APIs

	dal_ALmixer_OutputDecoders ALmixer_OutputDecoders;
	dal_ALmixer_OutputOpenALInfo ALmixer_OutputOpenALInfo;
	dal_ALmixer_CompiledWithThreadBackend ALmixer_CompiledWithThreadBackend;
}

class DerelictDALmixerLoader : SharedLibLoader {
	public this() {
		super( libNames );
	}
	
	protected override void loadSymbols() {
		///CoreOperation Initialization, Tear-down, and Core Operational Commands
		bindFunc( cast( void** )&ALmixer_GetLinkedVersion, "ALmixer_GetLinkedVersion" );
		bindFunc( cast( void** )&ALmixer_GetError, "ALmixer_GetError" );
		bindFunc( cast( void** )&ALmixer_SetError, "ALmixer_SetError" );
		bindFunc( cast( void** )&ALmixer_GetTicks, "ALmixer_GetTicks" );
		bindFunc( cast( void** )&ALmixer_Delay, "ALmixer_Delay" );
		bindFunc( cast( void** )&ALmixer_Init, "ALmixer_Init" );
		bindFunc( cast( void** )&ALmixer_InitContext, "ALmixer_InitContext" );
		bindFunc( cast( void** )&ALmixer_InitMixer, "ALmixer_InitMixer" );
		bindFunc( cast( void** )&ALmixer_BeginInterruption, "ALmixer_BeginInterruption" );
		bindFunc( cast( void** )&ALmixer_EndInterruption, "ALmixer_EndInterruption" );
		bindFunc( cast( void** )&ALmixer_IsInInterruption, "ALmixer_IsInInterruption" );
		bindFunc( cast( void** )&ALmixer_SuspendUpdates, "ALmixer_SuspendUpdates" );
		bindFunc( cast( void** )&ALmixer_ResumeUpdates, "ALmixer_ResumeUpdates" );
		bindFunc( cast( void** )&ALmixer_AreUpdatesSuspended, "ALmixer_AreUpdatesSuspended" );
		bindFunc( cast( void** )&ALmixer_SuspendPlayingState, "ALmixer_SuspendPlayingState" );
		bindFunc( cast( void** )&ALmixer_ResumePlayingState, "ALmixer_ResumePlayingState" );
		bindFunc( cast( void** )&ALmixer_IsPlayingStateSuspended, "ALmixer_IsPlayingStateSuspended" );
		bindFunc( cast( void** )&ALmixer_Quit, "ALmixer_Quit" );
		bindFunc( cast( void** )&ALmixer_QuitWithoutFreeData, "ALmixer_QuitWithoutFreeData" );
		bindFunc( cast( void** )&ALmixer_IsInitialized, "ALmixer_IsInitialized" );
		bindFunc( cast( void** )&ALmixer_GetFrequency, "ALmixer_GetFrequency" );
		bindFunc( cast( void** )&ALmixer_AllocateChannels, "ALmixer_AllocateChannels" );
		bindFunc( cast( void** )&ALmixer_ReserveChannels, "ALmixer_ReserveChannels" );
		bindFunc( cast( void** )&ALmixer_Update, "ALmixer_Update" );
		///LoadAPI Load Audio Functions
		bindFunc( cast( void** )&ALmixer_LoadSample_RW, "ALmixer_LoadSample_RW" );
		bindFunc( cast( void** )&ALmixer_LoadStream_RW, "ALmixer_LoadStream_RW" );
		bindFunc( cast( void** )&ALmixer_LoadAll_RW, "ALmixer_LoadAll_RW" );
		bindFunc( cast( void** )&ALmixer_LoadSample, "ALmixer_LoadSample" );
		bindFunc( cast( void** )&ALmixer_LoadStream, "ALmixer_LoadStream" );
		bindFunc( cast( void** )&ALmixer_LoadAll, "ALmixer_LoadAll" );
		bindFunc( cast( void** )&ALmixer_LoadSample_RAW_RW, "ALmixer_LoadSample_RAW_RW" );
		bindFunc( cast( void** )&ALmixer_LoadStream_RAW_RW, "ALmixer_LoadStream_RAW_RW" );
		bindFunc( cast( void** )&ALmixer_LoadAll_RAW_RW, "ALmixer_LoadAll_RAW_RW" );
		bindFunc( cast( void** )&ALmixer_LoadSample_RAW, "ALmixer_LoadSample_RAW" );
		bindFunc( cast( void** )&ALmixer_LoadStream_RAW, "ALmixer_LoadStream_RAW" );
		bindFunc( cast( void** )&ALmixer_LoadAll_RAW, "ALmixer_LoadAll_RAW" );
		bindFunc( cast( void** )&ALmixer_FreeData, "ALmixer_FreeData" );
		bindFunc( cast( void** )&ALmixer_IsPredecoded, "ALmixer_IsPredecoded" );
		///CallbackAPI Callbacks
		bindFunc( cast( void** )&ALmixer_SetPlaybackFinishedCallback, "ALmixer_SetPlaybackFinishedCallback" );
		bindFunc( cast( void** )&ALmixer_SetPlaybackDataCallback, "ALmixer_SetPlaybackDataCallback" );
		///PlayAPI Functions useful for playback.
		bindFunc( cast( void** )&ALmixer_GetTotalTime, "ALmixer_GetTotalTime" );
		bindFunc( cast( void** )&ALmixer_GetSource, "ALmixer_GetSource" );
		bindFunc( cast( void** )&ALmixer_GetChannel, "ALmixer_GetChannel" );
		bindFunc( cast( void** )&ALmixer_FindFreeChannel, "ALmixer_FindFreeChannel" );
		bindFunc( cast( void** )&ALmixer_PlayChannelTimed, "ALmixer_PlayChannelTimed" );
		bindFunc( cast( void** )&ALmixer_PlayChannel, "ALmixer_PlayChannel" );
		bindFunc( cast( void** )&ALmixer_PlaySourceTimed, "ALmixer_PlaySourceTimed" );
		bindFunc( cast( void** )&ALmixer_PlaySource, "ALmixer_PlaySource" );
		bindFunc( cast( void** )&ALmixer_HaltChannel, "ALmixer_HaltChannel" );
		bindFunc( cast( void** )&ALmixer_HaltSource, "ALmixer_HaltSource" );
		bindFunc( cast( void** )&ALmixer_RewindData, "ALmixer_RewindData" );
		bindFunc( cast( void** )&ALmixer_RewindChannel, "ALmixer_RewindChannel" );
		bindFunc( cast( void** )&ALmixer_RewindSource, "ALmixer_RewindSource" );
		bindFunc( cast( void** )&ALmixer_SeekData, "ALmixer_SeekData" );
		bindFunc( cast( void** )&ALmixer_SeekChannel, "ALmixer_SeekChannel" );
		bindFunc( cast( void** )&ALmixer_SeekSource, "ALmixer_SeekSource" );
		bindFunc( cast( void** )&ALmixer_PauseChannel, "ALmixer_PauseChannel" );
		bindFunc( cast( void** )&ALmixer_PauseSource, "ALmixer_PauseSource" );
		bindFunc( cast( void** )&ALmixer_ResumeChannel, "ALmixer_ResumeChannel" );
		bindFunc( cast( void** )&ALmixer_ResumeSource, "ALmixer_ResumeSource" );
		bindFunc( cast( void** )&ALmixer_ExpireChannel, "ALmixer_ExpireChannel" );
		bindFunc( cast( void** )&ALmixer_ExpireSource, "ALmixer_ExpireSource" );
		///VolumeAPI Volume and Fading
		bindFunc( cast( void** )&ALmixer_FadeInChannelTimed, "ALmixer_FadeInChannelTimed" );
		bindFunc( cast( void** )&ALmixer_FadeInChannel, "ALmixer_FadeInChannel" );
		bindFunc( cast( void** )&ALmixer_FadeInSourceTimed, "ALmixer_FadeInSourceTimed" );
		bindFunc( cast( void** )&ALmixer_FadeInSource, "ALmixer_FadeInSource" );
		bindFunc( cast( void** )&ALmixer_FadeOutChannel, "ALmixer_FadeOutChannel" );
		bindFunc( cast( void** )&ALmixer_FadeOutSource, "ALmixer_FadeOutSource" );
		bindFunc( cast( void** )&ALmixer_FadeChannel, "ALmixer_FadeChannel" );
		bindFunc( cast( void** )&ALmixer_FadeSource, "ALmixer_FadeSource" );
		bindFunc( cast( void** )&ALmixer_SetVolumeChannel, "ALmixer_SetVolumeChannel" );
		bindFunc( cast( void** )&ALmixer_SetVolumeSource, "ALmixer_SetVolumeSource" );
		bindFunc( cast( void** )&ALmixer_GetVolumeChannel, "ALmixer_GetVolumeChannel" );
		bindFunc( cast( void** )&ALmixer_GetVolumeSource, "ALmixer_GetVolumeSource" );
		bindFunc( cast( void** )&ALmixer_SetMaxVolumeChannel, "ALmixer_SetMaxVolumeChannel" );
		bindFunc( cast( void** )&ALmixer_SetMaxVolumeSource, "ALmixer_SetMaxVolumeSource" );
		bindFunc( cast( void** )&ALmixer_GetMaxVolumeChannel, "ALmixer_GetMaxVolumeChannel" );
		bindFunc( cast( void** )&ALmixer_GetMaxVolumeSource, "ALmixer_GetMaxVolumeSource" );
		bindFunc( cast( void** )&ALmixer_SetMinVolumeChannel, "ALmixer_SetMinVolumeChannel" );
		bindFunc( cast( void** )&ALmixer_SetMinVolumeSource, "ALmixer_SetMinVolumeSource" );
		bindFunc( cast( void** )&ALmixer_GetMinVolumeChannel, "ALmixer_GetMinVolumeChannel" );
		bindFunc( cast( void** )&ALmixer_GetMinVolumeSource, "ALmixer_GetMinVolumeSource" );
		bindFunc( cast( void** )&ALmixer_SetMasterVolume, "ALmixer_SetMasterVolume" );
		bindFunc( cast( void** )&ALmixer_GetMasterVolume, "ALmixer_GetMasterVolume" );
		///QueryAPI Query APIs
		bindFunc( cast( void** )&ALmixer_IsActiveSource, "ALmixer_IsActiveSource" );
		bindFunc( cast( void** )&ALmixer_IsPlayingChannel, "ALmixer_IsPlayingChannel" );
		bindFunc( cast( void** )&ALmixer_IsPlayingSource, "ALmixer_IsPlayingSource" );
		bindFunc( cast( void** )&ALmixer_IsPausedChannel, "ALmixer_IsPausedChannel" );
		bindFunc( cast( void** )&ALmixer_IsPausedSource, "ALmixer_IsPausedSource" );
		bindFunc( cast( void** )&ALmixer_CountAllFreeChannels, "ALmixer_CountAllFreeChannels" );
		bindFunc( cast( void** )&ALmixer_CountUnreservedFreeChannels, "ALmixer_CountUnreservedFreeChannels" );
		bindFunc( cast( void** )&ALmixer_CountAllUsedChannels, "ALmixer_CountAllUsedChannels" );
		bindFunc( cast( void** )&ALmixer_CountUnreservedUsedChannels, "ALmixer_CountUnreservedUsedChannels" );
		bindFunc( cast( void** )&ALmixer_CountTotalChannels, "ALmixer_CountTotalChannels" );
		bindFunc( cast( void** )&ALmixer_CountReservedChannels, "ALmixer_CountReservedChannels" );
		///DebugAPI Debug APIs
		bindFunc( cast( void** )&ALmixer_OutputDecoders, "ALmixer_OutputDecoders" );
		bindFunc( cast( void** )&ALmixer_OutputOpenALInfo, "ALmixer_OutputOpenALInfo" );
		bindFunc( cast( void** )&ALmixer_CompiledWithThreadBackend, "ALmixer_CompiledWithThreadBackend" );
	}
}

__gshared DerelictDALmixerLoader DALmixer;

shared static this() {
	DALmixer = new DerelictDALmixerLoader();
}
